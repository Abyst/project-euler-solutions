
//! ```cargo
//! [package]
//! edition = "2021"
//! 
//! [dependencies]
//! num = "0.4"
//! ```

use num::Integer;

fn main() {
	let value = (1..20).fold(1, |a, b| { a.lcm(&b) });
	print!("{value}");
}

