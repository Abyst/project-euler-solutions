from itertools import takewhile

class Fibonacci:
	def __new__(self):
		a, b = 1, 0
		while True:
			a, b = b, a + b
			yield b

if __name__ == '__main__':
	limit = 4_000_000
	value = sum(filter(lambda n:
		n % 2 == 0,
		takewhile(lambda n:
			n < limit,
			Fibonacci())))
	
	print(value, end='')

