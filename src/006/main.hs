import Data.Function ((&))
import Data.Word

squaresSum :: Word -> Word
squaresSum top =
  top * (top + 1) * (2 * top + 1) `div` 6

sumSquares :: Word -> Word
sumSquares top =
  squares * squares
  where
    squares = top * (top + 1) `div` 2

main = do
  let
    top = 100
    value = sumSquares top - squaresSum top
  
  value & show & putStr

