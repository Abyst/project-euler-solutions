# include <stdbool.h>
# include <stdint.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>

# define auto __auto_type

typedef struct {
	size_t a;
	size_t b;
} Tuple;

inline
bool is_palindrome(const size_t number) {
	size_t reversed = 0;
	for (auto div = number; div != 0; div /= 10) {
		reversed = reversed * 10 + div % 10;
	}
	return number == reversed;
}

Tuple palindrome_multiplicands(const uint8_t n_digits) {
	const auto max = (size_t) pow(10.0, n_digits) - 1;
	
	const auto min_a = max * 9 / 10;
	for (auto a = max; a > min_a; a--) {
		const auto min_b = a * 9 / 10;
		for (auto b = a; b > min_b; b--) {
			if (is_palindrome(a * b)) {
				return (Tuple) { a, b };
			}
		}
	}
	
	fprintf(stderr, "Didn't return in loop\n");
	exit(1);
}

int main(void) {
	const auto m = palindrome_multiplicands(3);
	const auto value = m.a * m.b;
	printf("%zu", value);
}

