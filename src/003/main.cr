
class Factors
	include Enumerable(Int::Unsigned)
	
	def initialize(@number : Int::Unsigned)
	end
	
	def each(&)
		while @number % 2 == 0
			@number //= 2
			two : Int::Unsigned = 2
			yield two
		end
		
		(3u64..Math.sqrt(@number).to_i()).step(2) do |i|
			while @number % i == 0
				@number //= i
				yield i
			end
		end
		
		yield @number if @number > 2
	end
end

def largest_prime_factor(
	number : Int::Unsigned
) : Int::Unsigned
	Factors.new(number).select(){|_| true}.pop()
end

number = 600_851_475_143u64
value = largest_prime_factor(number)

print(value)

