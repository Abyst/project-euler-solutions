
def squares_sum []: int -> int {
	$in * ($in + 1) * (2 * $in + 1) / 6
}

def sum_squares []: int -> int {
	($in * ($in + 1) / 2) ** 2
}

let top = 100
let value = ($top | sum_squares) - ($top | squares_sum)

$value | print --no-newline

