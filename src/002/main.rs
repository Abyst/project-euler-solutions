
//! ```cargo
//! [package]
//! edition = "2021"
//! 
//! [dependencies]
//! num = "0.4"
//! ```

use num::{ Unsigned, PrimInt, Integer };

struct Fibonacci<U: Unsigned> {
	a: U,
	b: U,
}

impl<U: Unsigned + PrimInt> Fibonacci<U> {
	#[inline]
	#[must_use]
	fn new() -> Self {
		Fibonacci { a: U::one(), b: U::zero() }
	}
}

impl<U: Unsigned + PrimInt> Iterator for Fibonacci<U> {
	type Item = U;
	fn next(&mut self) -> Option<Self::Item> {
		let Self { a, b } = self;
		(*a, *b) = (*b, *a + *b);
		Some(*b)
	}
}

fn main() {
	let limit = 4_000_000;
	let value = Fibonacci::<usize>::new().take_while(|&i| {
		i < limit
	}).filter(Integer::is_even).sum::<usize>();
	
	print!("{value}");
}

