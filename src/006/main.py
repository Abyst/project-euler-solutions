
def squares_sum(top: int) -> int:
	return top * (top + 1) * (2 * top + 1) // 6

def sum_squares(top: int) -> int:
	return (top * (top + 1) // 2) ** 2

if __name__ == '__main__':
	top = 100
	value = sum_squares(top) - squares_sum(top)
	
	print(value, end='')

