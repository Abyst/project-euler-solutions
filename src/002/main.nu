
def fibonacci [] {
	generate { a: 0, b: 1 } {|fib|
		let a = $fib.a
		let b = $fib.b
		{ out: $a, next: { a: $b b: ($a + $b) } }
	}
}

let limit = 4_000_000
let value = fibonacci | take while {|i|
	$i < $limit
} | filter {|i|
	$i mod 2 == 0
} | math sum

$value | print --no-newline

