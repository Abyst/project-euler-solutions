
def multiples_sum(
	top : Int::Unsigned,
	numbers : Set(Int::Unsigned)
) : Int::Unsigned
	top.times().select(){|m|
		numbers.any?(){|n|
			(m % n).zero?()
		}
	}.sum()
end

value = multiples_sum(1_000u64, Set{3u64, 5u64})
print(value)

