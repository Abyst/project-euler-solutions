
def multiples_sum(top, numbers)
	(0...top).select(){|m|
		numbers.any?(){|n|
			(m % n).zero?()
		}
	}.sum()
end

if __FILE__ == $0
	value = multiples_sum(1_000, [3, 5])
	print(value)
end

