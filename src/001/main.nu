
def multiples-sum [
	top: int,
	numbers: list<int>
]: nothing -> int {
	..($top - 1) | filter {|m|
		$numbers | any {|n|
			$m mod $n == 0
		}
	} | math sum
}

let value = multiples-sum 1_000 [3 5]
$value | print --no-newline

