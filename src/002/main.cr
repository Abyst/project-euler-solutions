
class Fibonacci
	include Enumerable(Int::Unsigned)
	
	def each(&)
		a : Int::Unsigned = 1
		b : Int::Unsigned = 0
		loop do
			a, b = b, a + b
			yield b
		end
	end
end

limit = 4_000_000u64
value = Fibonacci.new().take_while(){|n|
	n < limit
}.select(&.even?()).sum()

print(value)

