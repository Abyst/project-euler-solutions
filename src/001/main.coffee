
multiplesSum = (top, numbers) ->
  [0...top].filter (m) ->
    numbers.some (n) ->
      m % n == 0
  .reduce (a, b) ->
    a + b

if require.main == module
  value = multiplesSum(1_000, [3, 5])
  process.stdout.write(String value)

