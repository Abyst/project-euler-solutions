
def is_palindrome(number: int) -> bool:
	reversed, quot = 0, number
	while quot != 0:
		quot, rem = divmod(quot, 10)
		reversed = reversed * 10 + rem
	return number == reversed

def get_multiplicands(n_digits: int) -> (int, int):
	max = 10 ** n_digits - 1
	
	for a in range(max, max * 9 // 10, -1):
		for b in range(a, a * 9 // 10, -1):
			if is_palindrome(a * b):
				return (a, b)
	
	raise RuntimeError("Didn't return in loop!")

if __name__ == "__main__":
	a, b = get_multiplicands(3)
	value = a * b
	
	print(value, end='')

