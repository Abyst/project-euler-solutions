require "prime"

def largest_prime_factor(number)
	Prime.prime_division(number).last().first()
end

if __FILE__ == $0
	number = 600_851_475_143
	value = largest_prime_factor(number)
	print(value)
end

