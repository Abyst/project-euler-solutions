
def squares_sum(top)
	top * (top + 1) * (2 * top + 1) / 6
end

def sum_squares(top)
	(top * (top + 1) / 2) ** 2
end

if __FILE__ == $0
	top = 100
	value = sum_squares(top) - squares_sum(top)
	
	print(value)
end

