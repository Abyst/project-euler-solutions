{-# LANGUAGE NumericUnderscores #-}

import Data.Function ((&))
import Data.Word

multiplesSum :: Word -> [Word] -> Word
multiplesSum top numbers =
  [0..top-1] & filter (\m ->
    numbers & any (\n ->
      m `mod` n == 0)) & sum

main = do
  let value = multiplesSum 1_000 [3, 5]
  value & show & putStr

