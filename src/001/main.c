# include <stddef.h>
# include <stdio.h>

# define auto __auto_type

# define ARRAY(type_, ...) \
	(type_[]) { __VA_ARGS__ }, \
	(sizeof((type_[]) { __VA_ARGS__ }) / sizeof(type_))

inline
size_t multiples_sum(
	const size_t top,
	const size_t numbers[],
	const size_t n_numbers
) {
	size_t sum = 0;
	for (size_t n = 0; n < top; n += 1) {
		for (size_t i = 0; i < n_numbers; i += 1) {
			if (n % numbers[i] == 0) {
				sum += n;
				break;
			}
		}
	}
	return sum;
}

int main(void) {
	const auto value = multiples_sum(
		1000,
		ARRAY(size_t, 3, 5)
	);
	printf("%zu", value);
}

