{-# LANGUAGE NumericUnderscores #-}

import Data.Function ((&))
import Data.Word

fibonacci :: Word -> Word -> [Word]
fibonacci a b =
  b:fibonacci b (a+b)

main = do
  let
    limit = 4_000_000
    value = fibonacci 1 0 &
      takeWhile (< limit) &
      filter even &
      sum
  value & show & putStr

