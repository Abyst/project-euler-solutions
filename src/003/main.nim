import std/sequtils
import std/math

type Factors = object
  number: uint

iterator items(iter: Factors): uint =
  var number = iter.number
  while number mod 2u == 0u:
    number = number div 2u
    yield 2u
  
  for i in countup(
    3u,
    uint(sqrt(float(number))),
    2u
  ):
    while number mod i == 0u:
      number = number div i
      yield i
  
  if number > 2u:
    yield number

func largest_prime_factor(number: uint): uint =
    Factors(number: number).toSeq()[^1]

if isMainModule:
  let number = 600_851_475_143u
  let value = largest_prime_factor(number)
  
  stdout.write(value)

