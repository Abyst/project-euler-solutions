const std = @import("std");
const out = std.io.getStdOut().writer();

inline
fn isPalindrome(comptime T: type, number: T) bool {
	var reversed: T = 0;
	var quot: T = number;
	while (quot != 0) : (quot /= 10) {
		reversed = reversed * 10 + quot % 10;
	}
	return number == reversed;
}

fn getMultiplicands(nDigits: u8) struct { a: u64, b: u64 } {
	const max = std.math.pow(u64, 10, nDigits) - 1;
	
	const min_a = max * 9 / 10;
	var a = max;
	while (a > min_a) : (a -= 1) {
		const min_b = a * 9 / 10;
		var b = a;
		while (b > min_b) : (b -= 1) {
			if (isPalindrome(u64, a * b)) {
				return .{ .a = a, .b = b };
			}
		}
	}
	
	unreachable;
}

pub fn main() !void {
	const m = getMultiplicands(3);
	const value = m.a * m.b;
	
	try out.print("{}", .{ value });
}

