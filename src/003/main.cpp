# include <range/v3/all.hpp>
# include <iostream>
# include <optional>
# include <cmath>

template<
	class Uint,
	class = std::enable_if<std::is_unsigned<Uint>::value>>
struct Factors {
public:
	static inline
	auto generator(const Uint number) {
		return ranges::views::generate([
			number = number,
			index = 2,
			root = 0
		]() mutable -> std::optional<Uint> {
			if (index == 2) {
				if (number % 2 == 0) {
					number /= 2;
					return std::optional(2);
				}
				root = std::sqrt(number);
				index = 3;
			}
			
			while (index <= root) {
				const auto old_index = index;
				index += 2;
				if (number % old_index == 0) {
					number /= old_index;
					return std::optional(old_index);
				}
			}
			
			if (number > 2) {
				const auto copy = std::optional(number);
				number = 0;
				return copy;
			}
			
			return std::nullopt;
		});
	}
};

int main(void) {
	const auto number = 600'851'475'143uz;
	const auto value = ranges::max(
		Factors<size_t>::generator(number) |
		ranges::views::take_while([](const auto i) {
			return i.has_value();
		}) |
		ranges::views::transform([](const auto i) {
			return i.value();
		})
	);
	
	std::cout << value;
}

