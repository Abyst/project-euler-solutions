const std = @import("std");
const out = std.io.getStdOut().writer();

const Fibonacci = struct {
	a: usize = 1,
	b: usize = 0,
	
	inline
	fn next(self: *Fibonacci) ?usize {
		const temp = self.a;
		self.a = self.b;
		self.b += temp;
		return self.b;
	}
};

inline
fn evenFibonacciNumbers(comptime T: type, limit: T) T {
	switch (@typeInfo(T)) {
		.Int => |info| {
			if (info.signedness == .signed) {
				@compileError("integer must be unsigned");
			}
		},
		else => {
			@compileError("must be an unsigned integer");
		},
	}
	var sum: T = 0;
	var it = Fibonacci{};
	while (it.next()) |n| {
		if (n > limit) {
			break;
		}
		if (n % 2 == 0) {
			sum += n;
		}
	}
	return sum;
}

pub fn main() !void {
	const limit = 4_000_000;
	const value = evenFibonacciNumbers(usize, limit);
	try out.print("{}", .{value});
}

