# include <range/v3/all.hpp>
# include <iostream>
# include <cmath>
# include <tuple>

using ranges::views::reverse;
using ranges::views::iota;

template<class Int>
inline
bool is_palindrome(Int number) {
	Int reversed = 0;
	for (auto quot = number; quot != 0; quot /= 10) {
		reversed = reversed * 10 + quot % 10;
	}
	return number == reversed;
}

template<class Int>
std::tuple<
	Int,
	Int
> get_multiplicands(uint8_t n_digits) {
	const Int max = std::pow(10, n_digits) - 1;
	
	for (auto a : iota(max * 9 / 10, max) | reverse) {
		for (auto b : iota(a * 9 / 10, a) | reverse) {
			if (is_palindrome(a * b)) {
				return { a, b };
			}
		}
	}
	
	throw "Didn't return in loop";
}

int main(void) {
	const auto [a, b] = get_multiplicands<size_t>(3);
	const auto value = a * b;
	std::cout << value;
}

