
function is_palindrome(number::Int64)::Bool
	reversed, quot = 0, number
	while quot != 0
		quot, rem = divrem(quot, 10)
		reversed = reversed * 10 + rem
	end
	number == reversed
end

function get_multiplicands(
	n_digits::Int64
)::Tuple{Int64, Int64}
	max = 10 ^ n_digits - 1
	
	for a in reverse(div(max * 9, 10):max)
		for b in reverse(div(a * 9, 10):a)
			if is_palindrome(a * b)
				return a, b
			end
		end
	end
	
	throw(error("Didn't return in loop!"))
end

if abspath(PROGRAM_FILE) == @__FILE__
	a, b = get_multiplicands(3)
	value = a * b
	
	print(value)
end

