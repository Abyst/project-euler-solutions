
func squares_sum(top: uint): uint =
  top * (top + 1) * (2 * top + 1) div 6

func sum_squares(top: uint): uint =
  let squares = top * (top + 1) div 2
  squares * squares

if isMainModule:
  let top = 100u
  let value = sum_squares(top) - squares_sum(top)
  
  stdout.write(value)

