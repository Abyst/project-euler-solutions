# include <stdio.h>

# define auto __auto_type

inline
size_t squares_sum(const size_t top) {
	return top * (top + 1) * (2 * top + 1) / 6;
}

inline
size_t sum_squares(const size_t top) {
	const auto squares = top * (top + 1) / 2;
	return squares * squares;
}

int main(void) {
	const auto top = (size_t) 100;
	const auto value = sum_squares(top) - squares_sum(top);
	
	printf("%zu", value);
}

