import Data.Function ((&))
import Data.Word

isPalindrome :: Word -> Bool
isPalindrome =
  (==) <*> read . reverse . show

getMultiplicands :: Word -> (Word, Word)
getMultiplicands nDigits =
  [(a, b) |
    a <- [(m * 9 `div` 10)..m] & reverse,
    b <- [(a * 9 `div` 10)..a] & reverse,
    a * b & isPalindrome] & head
  where
    m = 10 ^ nDigits - 1

main = do
  let
    (a, b) = getMultiplicands 3
    value = a * b
  
  value & show & putStr

