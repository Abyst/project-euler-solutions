
def run [path: string] {
	let cpp_flags = ([
		-lm
		-pedantic
		-pedantic-errors
		-W
		-Wall
		-Wextra
		-Werror
		-Wno-gnu-auto-type
		-Wno-keyword-macro
	] | str join ' ')
	
	let extension = ($path | path parse).extension
	let value = (match $extension {
		'awk' => { awk -f $path }
		'c' => { c $'($path) -std=c2x ($cpp_flags)' }
		'coffee' => { coffee $path }
		'cpp' => { c $'($path) -std=c++2b ($cpp_flags)' }
		'cr' => { crystal $path }
		'hs' => { runhaskell $path }
		'jl' => { julia $path }
		'nim' => { nim r -r --hints:off $path }
		'nu' => { nu $path }
		'py' => { python $path }
		'rb' => { ruby $path }
		'rs' => { cargo +nightly script --clear-cache $path }
		'zig' => { zig run $path }
	})
	{ language: $extension, value: $value}
}

export def all [dir: string] {
	let solution = (open $'($dir)/solution.txt')
	ls $'($dir)/main.*' | par-each {|file|
		(main $file.name $solution)
	}
}

export def main [file: string, solution: string] {
	let run = (run $file)
	$run | insert ok ($run.value == $solution)
}


