
Object.prototype.takeWhile = (predicate) ->
  result = []
  for item from this
    break unless predicate(item)
    result.push(item)
  result

class Fibonacci
  [Symbol.iterator]: ->
    [a, b] = [1, 0]
    next: () ->
      [a, b] = [b, a + b]
      { done: false, value: b }

if require.main == module
  limit = 4_000_000
  value = new Fibonacci()
    .takeWhile (n) ->
      n < limit
    .filter (n) ->
      n % 2 == 0
    .reduce (a, b) ->
      a + b
  
  process.stdout.write(String value)

