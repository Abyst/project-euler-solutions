# include <stdbool.h>
# include <stdio.h>

inline
size_t gcd(const size_t a, const size_t b) {
	return b == 0 ? a : gcd(b, a % b);
}

inline
size_t lcm(const size_t a, const size_t b) {
	return b / gcd(a, b) * a;
}

int main(void) {
	const size_t value = __extension__({
		size_t max = 1;
		for (size_t i = 1; i <= 20; i++) {
			max = lcm(max, i);
		}
		max;
	});
	
	printf("%zu", value);
}

