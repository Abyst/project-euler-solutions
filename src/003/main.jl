import Pkg
Pkg.add("Primes")

import Primes

function largest_prime_factor(number::UInt64)::UInt64
	Primes.factor(number).pe |> last |> first
end

if abspath(PROGRAM_FILE) == @__FILE__
	const number = UInt64(600_851_475_143)
	const value = largest_prime_factor(number)
	
	value |> print
end

