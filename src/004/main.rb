
def is_palindrome number
	reversed, quot = 0, number
	while quot != 0
		quot, rem = quot.divmod 10
		reversed = reversed * 10 + rem
	end
	number == reversed
end

def get_multiplicands n_digits
	max = 10 ** n_digits - 1
	for a in max.downto(max * 9 / 10)
		for b in a.downto(a * 9 / 10)
			if is_palindrome a * b
				return a, b
			end
		end
	end
	
	raise "Didn't return in loop!"
end

if __FILE__ == $0
	a, b = get_multiplicands(3)
	value = a * b
	
	print(value)
end

