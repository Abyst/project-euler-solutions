import std/sequtils
import std/sugar

proc multiplesSum(top: uint, numbers: seq[uint]): uint =
  toSeq(0u ..< top).filter(m =>
    numbers.any(n =>
      m mod n == 0)).foldl(a + b)

when isMainModule:
  let value = multiplesSum(1_000u, @[3u, 5u])
  stdout.write(value)
  
