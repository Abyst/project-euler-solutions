
function multiples_sum(
	top::UInt64,
	numbers::Set{UInt64}
)::UInt64
	(0 : top - 1) |> filter(m ->
		any(n ->
			m % n == 0,
			numbers)
		) |> sum
end

if abspath(PROGRAM_FILE) == @__FILE__
	value = multiples_sum(
		UInt64(1_000),
		Set([UInt64(3), 5])
	)
	value |> print
end

