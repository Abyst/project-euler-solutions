import Data.Function ((&))
import Data.Word

main = do
  let value = [1..20] & foldl lcm 1
  value & show & putStr

