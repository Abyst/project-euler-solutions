import std/sequtils
import std/math

when isMainModule:
  const value = (1..20).toSeq().foldl(lcm(a, b))
  stdout.write(value)

