const std = @import("std");
const out = std.io.getStdOut().writer();

inline
fn multiplesSum(
	comptime T: type,
	top: T,
	numbers: []const T
) T {
	switch (@typeInfo(T)) {
		.Int => |info| {
			if (info.signedness == .signed) {
				@compileError("integer must be unsigned");
			}
		},
		else => {
			@compileError("must be an unsigned integer");
		},
	}
	var sum: T = 0;
	for (0 .. top) |m| {
		for (numbers) |n| {
			if (m % n == 0) {
				sum += m;
				break;
			}
		}
	}
	return sum;
}

pub
fn main() !void {
	const value = multiplesSum(
		usize,
		1_000,
		&[_]usize{3, 5}
	);
	try out.print("{}", .{value});
}

