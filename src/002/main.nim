import std/sequtils
import std/sugar

type Fibonacci = object

iterator items(iter: Fibonacci): uint =
  var (a, b) = (1u, 0u)
  while true:
    (a, b) = (b, a + b)
    yield b

iterator takeWhile(
  iter: Fibonacci,
  predicate: proc (n: uint): bool
): uint =
  for fib in iter:
    if not predicate(fib):
      break
    yield fib

when isMainModule:
  const limit = 4_000_000
  const value = Fibonacci().takeWhile(n =>
    n < limit).toSeq().filter(n =>
      n mod 2 == 0).foldl(a + b)
  
  stdout.write(value)

