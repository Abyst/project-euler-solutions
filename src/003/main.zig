const std = @import("std");
const out = std.io.getStdOut().writer();
const math = std.math;

fn Factors(comptime U: type) type {
	switch (@typeInfo(U)) {
		.Int => |info| {
			if (info.signedness == .signed) {
				@compileError("integer must be unsigned");
			}
		},
		else => {
			@compileError("must be an unsigned integer");
		},
	}
	
	return struct {
		number: U,
		index: U,
		top: U,
		
		inline
		fn init(number: U) Factors(U) {
			return Factors(U) {
				.number = number,
				.index = 2,
				.top = 0
			};
		}
		
		inline
		fn next(self: *Factors(U)) ?U {
			if (self.index == 2) {
				if (self.number % 2 == 0) {
					self.number /= 2;
					return 2;
				}
				self.top = math.sqrt(self.number);
				self.index = 3;
			}
			
			while (self.index <= self.top) {
				const index = self.index;
				self.index += 2;
				if (self.number % index == 0) {
					self.number /= index;
					return index;
				}
			}
			
			if (self.number > 2) {
				const ret = self.number;
				self.number = 0;
				return ret;
			}
			
			return null;
		}
	};
}

inline
fn largestPrimeFactor(comptime U: type, number: U) U {
	var it = Factors(U).init(number);
	var largest: U = 0;
	while (it.next()) |f| {
		largest = f;
	}
	return largest;
}

pub fn main() !void {
	const number = 600_851_475_143;
	const value = block: {
		var largest: usize = 0;
		var it = Factors(usize).init(number);
		while (it.next()) |f| {
			largest = f;
		}
		break :block largest;
	};
	
	try out.print("{}", .{value});
}

