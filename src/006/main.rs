
//! ```cargo
//! [package]
//! edition = "2021"
//! 
//! [dependencies]
//! num = "0.4"
//! ```

use num::Unsigned;

#[inline]
fn squares_sum<U>(top: U) -> U
where U: Unsigned + Copy + From<u8>
{
	top *
	(top + U::from(1)) *
	(U::from(2) * top + U::from(1)) /
	U::from(6)
}

#[inline]
fn sum_squares<U>(top: U) -> U
where U: Unsigned + Copy + From<u8>
{
	let squares = top * (top + U::from(1)) / U::from(2);
	squares * squares
}

fn main() {
	let top = 100usize;
	let value = sum_squares(top) - squares_sum(top);
	
	print!("{value}");
}

