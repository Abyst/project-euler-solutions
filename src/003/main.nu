
def factors []: int -> list<int> {
	^factor $in | split row ' ' | skip | each {|value|
		$value | into int
	}
}

def largest-prime-factors [] {
	factors | last
}

let number = 600_851_475_143
let value = $number | largest-prime-factors

$value | print --no-newline

