import math

def factors(number: int) -> int:
	while number % 2 == 0:
		number //= 2
		yield 2
	
	for i in range(3, int(math.sqrt(number)), 2):
		while number % i == 0:
			number //= i
			yield i
	
	if number > 2:
		yield number

def largest_prime_factor(number: int) -> int:
	return list(factors(number))[-1]

if __name__ == '__main__':
	number = 600_851_475_143
	value = largest_prime_factor(number)
	print(value, end='')

