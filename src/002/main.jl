
struct Fibonacci
end

function Base.iterate(iter::Fibonacci, (a, b)=(1, 0))
	a + b, (b, a + b)
end

if abspath(PROGRAM_FILE) == @__FILE__
	const limit = UInt64(4_000_000)
	const value = Fibonacci() |>
		x -> Iterators.takewhile(<(limit), x) |>
		x -> Iterators.filter(isodd, x) |>
		sum
	value |> print
end

