
def 'math gcd' [b: int]: int -> int {
	let a = $in
	if $b == 0 {
		$a
	} else {
		$b | math gcd ($a mod $b)
	}
}

def 'math lcm' [b: int]: int -> int {
	let a = $in
	$b // ($a | math gcd $b) * $a
}

let value = 1..20 | reduce {|a, b| $a | math lcm $b }
$value | print --no-newline

