const std = @import("std");
const out = std.io.getStdOut().writer();

inline fn squaresSum(comptime T: type, top: T) T {
	return top * (top + 1) * (2 * top + 1) / 6;
}

inline fn sumSquares(comptime T: type, top: T) T {
	const squares = top * (top + 1) / 2;
	return squares * squares;
}

pub fn main() !void {
	const top = 100;
	const value =
		sumSquares(usize, top) -
		squaresSum(usize, top);
	
	try out.print("{}", .{ value });
}

