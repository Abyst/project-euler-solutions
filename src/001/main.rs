
//! ```cargo
//! [package]
//! edition = "2021"
//! 
//! [dependencies]
//! num = "0.4"
//! ```

#![feature(step_trait)]
use num::{ Unsigned, Bounded };
use std::iter::{ Step, Sum };

#[inline]
#[must_use]
fn multiples_sum<U>(top: U, numbers: &[U]) -> U
where
	U: Unsigned + Bounded + Copy + Step + Sum
{
	(U::min_value()..top).filter(|&m| {
		numbers.iter().any(|&n| {
			m % n == U::min_value()
		})
	}).sum()
}

fn main() {
	let value = multiples_sum(1_000usize, &[3, 5]);
	print!("{value}");
}

