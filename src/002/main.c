# include <stdbool.h>
# include <stddef.h>
# include <stdio.h>

# define auto __auto_type

typedef struct {
	size_t a;
	size_t b;
} Fibonacci;

inline
Fibonacci Fibonacci_new() {
	return (Fibonacci) {
		.a = 1,
		.b = 0,
	};
}

inline
size_t Fibonacci_next(Fibonacci *this) {
	const auto temp = this->a;
	this->a = this->b;
	this->b += temp;
	return this->b;
}

inline
size_t Fibonacci_current(Fibonacci *const this) {
	return this->b;
}

int main(void) {
	const size_t limit = 4000000;
	printf("%zu", __extension__({
		Fibonacci it = Fibonacci_new();
		size_t sum = 0;
		for (
			size_t f;
			(f = Fibonacci_next(&it)) < limit;
		) {
			if (f % 2 == 0) {
				sum += f;
			}
		}
		sum;
	}));
}

