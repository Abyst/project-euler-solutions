
//! ```cargo
//! [package]
//! edition = "2021"
//! 
//! [dependencies]
//! num = "0.4"
//! ```

use num::{ Unsigned, PrimInt };
use num::traits::NumAssign;
use num::integer::Roots;

struct Factors<U: Unsigned> {
	number: U,
	index: U,
	top: U,
}

impl<U: Unsigned + PrimInt> Factors<U> {
	#[inline]
	#[must_use]
	fn new(number: U) -> Self {
		Self {
			number,
			index: U::one() + U::one(),
			top: U::zero(),
		}
	}
}

impl<U> Iterator for Factors<U>
where
	U: Unsigned + PrimInt + NumAssign + Roots
{
	type Item = U;
	
	fn next(&mut self) -> Option<Self::Item> {
		let two = U::one() + U::one();
		
		if self.index == two {
			if self.number % two == U::zero() {
				self.number /= two;
				return Some(two);
			}
			self.top = self.number.sqrt();
			self.index += U::one();
		}
		
		while self.index <= self.top {
			let index = self.index;
			self.index += two;
			if self.number % index == U::zero() {
				self.number /= index;
				return Some(index);
			}
		}
		
		if self.number > two {
			let out = self.number;
			self.number = U::zero();
			return Some(out);
		}
		
		None
	}
}

fn main() {
	let number = 600_851_475_143usize;
	let value = Factors::new(number).fold(0, |_, f| f);
	
	print!("{value}");
}

