
isPalindrome = (number) ->
	[reversed, quot] = [0, number]
	while quot != 0
		reversed = reversed * 10 + quot % 10
		quot //= 10
	number == reversed

palindromeMultiplicands = (n_digits) ->
	max = 10 ** n_digits - 1
	
	for a in [max..(max * 9 / 10)]
		for b in [a..(a * 9 / 10)]
			if isPalindrome(a * b)
				return [a, b]
	
	throw "Didn't return in loop!"

if require.main == module
	[a, b] = palindromeMultiplicands(3)
	value = a * b
	process.stdout.write(String value)

