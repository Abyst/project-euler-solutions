{-# LANGUAGE NumericUnderscores #-}

import Data.Function ((&))
import Data.Word

primeFactors :: Word -> [Word]
primeFactors n =
  case factors of
    [] -> [n]
    _  -> factors ++ primeFactors (n `div` head factors)
  where
    isDivisible x = (n `mod` x) == 0
    factors = filter isDivisible [2 .. n-1] & take 1

largestPrimeFactor :: Word -> Word
largestPrimeFactor number =
  primeFactors number & last

main = do
  let
    number = 600_851_475_143
    value = largestPrimeFactor number
  
  value & show & putStr

