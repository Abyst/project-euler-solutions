# include <range/v3/all.hpp>
# include <iostream>
# include <numeric>

int main(void) {
	const auto value = ranges::fold_left(
		ranges::views::iota(1uz, 20uz),
		1,
		[](const auto a, const auto b) {
			return std::lcm(a, b);
		}
	);
	
	std::cout << value;
}

