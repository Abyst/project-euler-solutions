
def multiples_sum(top: int, numbers: set[int]) -> int:
	return sum(filter(lambda m:
		any(map(lambda n:
			m % n == 0, numbers)), range(top)))

if __name__ == '__main__':
	value = multiples_sum(1_000, [3, 5])
	print(value, end='')

