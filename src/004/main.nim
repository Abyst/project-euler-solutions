import std/math

func isPalindrome(number: uint64): bool =
  var reversed = 0u64
  var quot = number
  while quot != 0:
    reversed = reversed * 10 + quot mod 10
    quot = quot div 10
  return number == reversed

func getMultiplicands(nDigits: uint64): (uint64, uint64) =
  let m = 10u64 ^ nDigits - 1
  
  for a in countdown(m, m * 9 div 10):
    for b in countdown(a, a * 9 div 10):
      if isPalindrome(a * b):
        return (a, b)
  
  assert false, "Didn't return in loop"

when isMainModule:
  let (a, b) = getMultiplicands(3)
  let value = a * b
  
  stdout.write(value)

