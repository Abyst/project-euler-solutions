const std = @import("std");
const out = std.io.getStdOut().writer();

inline
fn lcm(comptime T: type, a: T, b: T) T {
	return b / std.math.gcd(a, b) * a;
}

pub
fn main() !void {
	const value = block: {
		var max: usize = 1;
		for (1 .. 20) |i| {
			max = lcm(usize, max, i);
		}
		break :block max;
	};
	try out.print("{}", .{ value });
}

