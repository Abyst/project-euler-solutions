
factors = (number) ->
  while number % 2 == 0
    number /= 2
    yield 2
  
  for i in [3..(Math.sqrt number)] by 2
    while number % i == 0
      number /= i
      yield i
  
  if number > 2
    yield number

largest_prime_factor = (number) ->

if require.main == module
  number = 600_851_475_143
  value = (n for n from factors(number)).pop()
  process.stdout.write(value.toString())

