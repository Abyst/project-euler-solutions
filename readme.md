# Project Euler Solutions

[Project Euler](https://projecteuler.net/about) problems and
solutions in many languages.

## Copyright

All `readme.md` files contain text extracted from the page
and are licensed under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Also read [copyright](https://projecteuler.net/copyright).

The code in this repository has no license, use it however
you please.

## Testing

```nu
$ use test.nu
$ test all src/001
```

