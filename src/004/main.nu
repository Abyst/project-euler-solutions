
def is-palindrome []: int -> bool {
	let number = $in | into string
	$number == ($number | str reverse)
}

def get-multiplicands [
	n_digits: int
]: nothing -> record<a: int, b: int> {
	let m = 10 ** $n_digits - 1
	
	for a in $m..($m * 9 // 10) {
		for b in $a..($a * 9 // 10) {
			if ($a * $b | is-palindrome) {
				return { a: $a, b: $b }
			}
		}
	}
	
	error make { msg: "Didn't return in loop" }
	{ a: 0, b: 0 }
}

let m = get-multiplicands 3
let value = $m.a * $m.b

$m.a * $m.b | print --no-newline

