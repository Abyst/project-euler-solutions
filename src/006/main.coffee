
squaresSum = (top) ->
  top * (top + 1) * (2 * top + 1) // 6

sumSquares = (top) ->
  (top * (top + 1) / 2) ** 2

if require.main == module
  top = 100
  value = sumSquares(top) - squaresSum(top)
  
  process.stdout.write(String value)

