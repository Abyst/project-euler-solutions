
class Fibonacci
	include Enumerable
	
	def each(&block)
		a, b = 1, 0
		loop do
			a, b = b, a + b
			yield b
		end
	end
end

if __FILE__ == $0
	limit = 4_000_000
	value = Fibonacci::new().take_while(){|n|
		n < limit
	}.select(&:even?).sum()
	
	print(value)
end

