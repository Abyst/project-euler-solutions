# include <range/v3/all.hpp>
# include <iostream>
# include <array>

template<
	class Uint,
	const size_t SIZE,
	class = std::enable_if<std::is_unsigned<Uint>::value>>
inline
Uint multiples_sum(
	const Uint top,
	std::array<Uint, SIZE> numbers
) {
	return ranges::accumulate(
		ranges::views::iota(Uint(0), top) |
		ranges::views::filter([numbers] (const Uint m) {
			return ranges::any_of(
				numbers,
				[m](const Uint n) {
					return m % n == 0;
				}
			);
		}),
		0
	);
}

int main(void) {
	const auto value = multiples_sum(
		1'000uz,
		std::array{3uz, 5uz}
	);
	std::cout << value;
}

