# include <iostream>

template<
	class Uint,
	class = std::enable_if<std::is_unsigned<Uint>::value>>
inline
Uint squares_sum(const Uint top) {
	return top * (top + 1) * (2 * top + 1) / 6;
}

template<
	class Uint,
	class = std::enable_if<std::is_unsigned<Uint>::value>>
inline
Uint sum_squares(const Uint top) {
	const auto squares = top * (top + 1) / 2;
	return squares * squares;
}

int main(void) {
	const auto top = 100uz;
	const auto value = sum_squares(top) - squares_sum(top);
	
	std::cout << value;
}

