
def is_palindrome(number : UInt64) : Bool
	reversed, temp = 0, number
	while temp != 0
		reversed = reversed * 10 + temp % 10
		temp //= 10
	end
	number == reversed
end

def get_multiplicands(n_digits : UInt64) : {UInt64, UInt64}
	max = 10u64 ** n_digits - 1
	
	(max..max * 9 / 10).step(-1) do |a|
		(a..a * 9 / 10).step(-1) do |b|
			if is_palindrome(a * b)
				return {a, b}
			end
		end
	end
	
	raise "Didn't return in loop!"
end

a, b = get_multiplicands(3)
value = a * b

print(value)


