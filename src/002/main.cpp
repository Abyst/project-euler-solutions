# include <range/v3/all.hpp>
# include <iostream>

template<
	class UInt,
	class = std::enable_if<std::is_unsigned<UInt>::value>>
struct Fibonacci {
public:
	static inline
	auto generator() {
		return ranges::views::generate([
			a = 1,
			b = 0
		]() mutable -> UInt {
			auto temp = a;
			a = b;
			b = b + temp;
			return b;
		});
	}
};

int main(void) {
	const auto limit = 4'000'000uz;
	const auto value = ranges::accumulate(
		Fibonacci<size_t>::generator() |
		ranges::views::take_while([limit](const auto i) {
			return i < limit;
		}) |
		ranges::views::filter([](const auto i) {
			return i % 2 == 0;
		}),
		0
	);
	
	std::cout << value;
}

