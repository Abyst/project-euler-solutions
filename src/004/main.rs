
#[inline]
fn is_palindrome(number: u64) -> bool {
	let (mut reversed, mut quot) = (0, number);
	while quot != 0 {
		reversed = reversed * 10 + quot % 10;
		quot /= 10;
	}
	number == reversed
}

fn get_multiplicands(n_digits: u8) -> (u64, u64) {
	let largest = 10u64.pow(n_digits.into()) - 1;
	
	for a in ((largest * 9 / 10)..largest).rev() {
		for b in ((a * 9 / 10)..a).rev() {
			if is_palindrome(a * b) {
				return (a, b);
			}
		}
	}
	
	unreachable!();
}

fn main() {
	let (a, b) = get_multiplicands(3);
	let value = a * b;
	
	print!("{value}");
}

