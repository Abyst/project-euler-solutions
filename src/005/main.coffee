
Number::gcd = (n) ->
  if n
    n.gcd(this % n)
  else
    this

Number::lcm = (n) ->
  n / this.gcd(n) * this

if require.main == module
  value = [1..20]
    .reduce (a, b) ->
      a.lcm(b)
  process.stdout.write(String value)

