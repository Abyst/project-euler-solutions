# include <stdbool.h>
# include <stddef.h>
# include <stdio.h>
# include <math.h>

# define auto __auto_type

typedef struct {
	size_t value;
	bool is_some;
} OptionalU64;

inline
OptionalU64 OptionalU64_Some(const size_t value) {
	return (OptionalU64) {
		.value = value,
		.is_some = true,
	};
}

inline
OptionalU64 OptionalU64_None() {
	return (OptionalU64) {
		.value = 0,
		.is_some = false,
	};
}

typedef struct {
	size_t number;
	size_t index;
	size_t top;
} Factors;

inline
Factors Factors_new(const size_t number) {
	return (Factors) {
		.number = number,
		.index = 2,
		.top = 0,
	};
}

inline
OptionalU64 Factors_next(Factors *this) {
	if (this->index == 2) {
		if (this->number % 2 == 0) {
			this->number /= 2;
			return OptionalU64_Some(2);
		}
		this->top = sqrt((double) this->number);
		this->index = 3;
	}
	
	while (this->index <= this->top) {
		const auto index = this->index;
		this->index += 2;
		if (this->number % index == 0) {
			this->number /= index;
			return OptionalU64_Some(index);
		}
	}
	
	if (this->number > 2) {
		size_t number = this->number;
		this->number = 0;
		return OptionalU64_Some(number);
	}
	
	return OptionalU64_None();
}

int main(void) {
	const auto number = (size_t) 600851475143;
	const auto largest = __extension__({
		size_t largest = 0;
		Factors it = Factors_new(number);
		for (
			OptionalU64 f;
			(f = Factors_next(&it)).is_some;
		) {
			largest = f.value;
		}
		largest;
	});
	
	printf("%zu", largest);
}

