
function squares_sum(top::UInt64)::UInt64
	top * (top + 1) * (2 * top + 1) ÷ 6
end

function sum_squares(top::UInt64)::UInt64
	squares = (top * (top + 1) ÷ 2)
	squares * squares
end

if abspath(PROGRAM_FILE) == @__FILE__
	const top = UInt64(100)
	const value = sum_squares(top) - squares_sum(top)
	
	value |> print
end

